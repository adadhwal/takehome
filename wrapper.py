import csv
from User import User
from Parser import Parser


def openFile(inputfile):
    with open(inputfile, newline='') as file:
        dialect = csv.Sniffer().sniff(file.read(1024))
    csvfile = open(inputfile, newline='')
    reader = csv.DictReader(csvfile, dialect=dialect)
    return reader


def extractData(users, fileReader):
    for row in fileReader:
        user = User()
        user.row = row
        users.append(user)

        parser = Parser(user)
        parser.parseAll()

    for user in users:
        parser = Parser(user)
        user.mentor = parser.lookUpMentor(user.mentor, users)


def saveFile(users, outputfile):
    with open(outputfile, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        fields = ['First Name', 'Last Name', 'Email Address', 'Mentor', 'Date of Last Game', 'High Score']
        writer.writerow(fields)
        users.sort(key=lambda x: int(x.high.replace(',', '')), reverse=True)
        for user in users:
            writer.writerow(user.getMembersAsArray())


def runImporter(inputfile, outputfile):
    users = list()

    fileReader = openFile(inputfile)

    extractData(users, fileReader)

    saveFile(users, outputfile)

    print('The records were written to ' + outputfile)

