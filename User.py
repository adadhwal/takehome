class User:

    def __init__(self):
        self.first = ''
        self.last = ''
        self.high = ''
        self.mentor = ''
        self.email = None
        self.date_last_game = ''
        self.row = dict()

    def __str__(self):
        return 'First: ' + self.first + '\n' + 'Last: ' + self.last + '\n' + 'High: ' + str(
            self.high) + '\n' + 'Mentor: ' + str(self.mentor).title() + '\n' + 'Email: ' + str(
            self.email) + '\n' + 'Date: ' + self.date_last_game

    # return all member fields as arrays
    def getMembersAsArray(self):
        arr = list()
        arr.append(self.first)
        arr.append(self.last)
        arr.append(self.email)
        arr.append(self.mentor)
        arr.append(self.date_last_game)
        arr.append(self.high)
        return arr