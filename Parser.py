import re
import calendar
import datetime


class Parser:
    # Regex Patterns

    # Highscore
    reHighScore = '(\d+)([km])'
    patternHighScore = re.compile(reHighScore, re.IGNORECASE | re.DOTALL)

    # Email
    reEmail = ".*(<)?([\w-]+@([\w-]+\.)+[\w-]+)(>)?$"
    patternEmail = re.compile(reEmail)

    # Date parsing regex from https://txt2re.com
    re1 = '((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))'  # Month 1
    re2 = '.*?'  # Non-greedy match on filler
    re3 = '((?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])'  # Day 1
    re4 = '.*?'  # Non-greedy match on filler
    re5 = '((?:(?:[1]{1}\\d{1}\\d{1}\\d{1})|(?:[2]{1}\\d{3})))(?![\\d])'  # Year 1
    patternDate1 = re.compile(re1 + re2 + re3 + re4 + re5, re.IGNORECASE | re.DOTALL)
    re12 = '((?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])'  # Month format 2
    patternDate2 = re.compile(re12 + re2 + re3 + re4 + re5, re.IGNORECASE | re.DOTALL)

    def __init__(self, user):
        self.user = user

    def parseAll(self):
        self.extractName()
        self.extractEmail()
        self.extractMentor()
        self.extractDate()
        self.extractHighScore()

    # helper functions for retrieving names
    def split_and_extract_name(self, header):
        first_name = ''
        last_name = ''
        if self.checkEmail(self.user.row[header], True) is True:
            splitted_string = self.user.row[header].split(' ')
            if len(splitted_string) >= 4:
                first_name = splitted_string[0] + ' ' + splitted_string[1]
                last_name = splitted_string[2]
            else:
                first_name = splitted_string[0]
                last_name = splitted_string[1]
        else:
            splitted_string = self.user.row[header].split(', ')
            first_name = splitted_string[1]
            last_name = splitted_string[0]

        return first_name, last_name

    def extract_name_2_columns(self, header_first, header_last):
        first_name = self.user.row[header_first]
        last_name = self.user.row[header_last]

        return first_name, last_name

    # retrieve and set name fields
    def extractName(self):
        first_name = ''
        last_name = ''
        if 'First' in self.user.row or 'Last' in self.user.row:
            first_name, last_name = self.extract_name_2_columns('First', 'Last')
        elif 'First Name' in self.user.row or 'Last Name' in self.user.row:
            first_name, last_name = self.extract_name_2_columns('First Name', 'Last Name')
        elif 'Player' in self.user.row:
            first_name, last_name = self.split_and_extract_name('Player')
        elif 'Name' in self.user.row:
            first_name, last_name = self.split_and_extract_name('Name')

        self.user.first = first_name.title()
        self.user.last = last_name.title()

    # check if current string is an email
    def checkEmail(self, email_str, set_user):

        matched_email = self.patternEmail.match(email_str)
        if matched_email is not None:
            if set_user is True:
                email = matched_email.group()
                if '<' in email:
                    pos_1 = email.find('<')
                    pos_2 = email.find('>')
                    email = email[pos_1 + 1:pos_2]
                self.user.email = email
            return True
        return False

    # retrieve and set email
    def extractEmail(self):
        if self.user.email is None:
            if 'Email' in self.user.row:
                self.checkEmail(self.user.row['Email'], True)
            elif 'Email Address' in self.user.row:
                self.checkEmail(self.user.row['Email Address'], True)

    # retrieve and set mentor field
    def extractMentor(self):
        mentor = ''
        if 'Mentor' in self.user.row:
            mentor = self.user.row['Mentor']
            if not self.checkEmail(mentor, False):
                if ',' in mentor:
                    splitted_string = self.user.row['Mentor'].split(', ')
                    mentor = splitted_string[1] + ' ' + splitted_string[0]
                    mentor = mentor.title()

        self.user.mentor = mentor

    # retrieve and set high score on user object
    def extractHighScore(self):
        max_score = 0

        header = ''

        if 'High Score' in self.user.row:
            header = 'High Score'
        elif 'Max Score' in self.user.row:
            header = 'Max Score'
        elif 'Highest Score' in self.user.row:
            header = 'Highest Score'

        if 'k' in self.user.row[header].lower() or 'm' in self.user.row[header].lower():
            matched_score = self.patternHighScore.search(self.user.row[header])
            if matched_score is not None:
                if matched_score[2].lower() == 'k':
                    max_score += int(matched_score[1]) * 1000
                elif matched_score[2].lower() == 'm':
                    max_score += int(matched_score[1]) * 1000000
            max_score = str(max_score)
        else:
            max_score = self.user.row[header]

        max_score = int(max_score.replace(',', ''))

        # format high score
        self.user.high = "{:,}".format(max_score)

    def extractDate(self):

        header = ''

        if 'Most Recent Game Date' in self.user.row:
            header = 'Most Recent Game Date'
        elif 'Date of Last Game' in self.user.row:
            header = 'Date of Last Game'
        elif 'Last Game Date' in self.user.row:
            header = 'Last Game Date'

        # Look for date
        found_date = self.patternDate1.search(self.user.row[header])

        # Check date format 1
        if found_date is not None:
            month = found_date[1]
            day = int(found_date[2])
            year = int(found_date[3])
            month = list(calendar.month_abbr).index(month.strip()[:3].capitalize())

            date = datetime.date(year, month, day).strftime('%b %d, %Y')
            self.user.date_last_game = date
        # Otherwise use other date format to parse
        else:
            found_date = self.patternDate2.search(self.user.row[header])
            month = int(found_date[1])
            day = int(found_date[2])
            year = int(found_date[3])

            date = datetime.date(year, month, day).strftime('%b %d, %Y')
            self.user.date_last_game = date

    # Find mentor's name given email, also format the string correctly
    def lookUpMentor(self, mentor, users):
        if self.checkEmail(mentor, False):
            for user in users:
                if user.email == mentor:
                    mentor = user.first + ' ' + user.last

        mentor = mentor.title()
        return mentor

