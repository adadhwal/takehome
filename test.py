import csv
from User import User
from Parser import Parser
if __name__ == '__main__':
    users = list()
    file_name = 'files/input3.tsv'
    with open(file_name, newline='') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
    with open(file_name, newline='') as csvfile:
        spamreader = csv.DictReader(csvfile, dialect=dialect)
        headers = spamreader.fieldnames



        for row in spamreader:
            keys = list(row.keys())
            user = User()
            user.row = row
            users.append(user)
        full_name = []
        for i in users:
            p = Parser(i)
            p.parseAll(i)
        for i in users:
            i.mentor = p.lookUpMentor(i.mentor, users)

            print(i)
    with open('output.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',')
        fields = ['First Name', 'Last Name', 'Email Address', 'Mentor', 'Date of Last Game', 'High Score']
        spamwriter.writerow(fields)
        users.sort(key=lambda x: int(x.high.replace(',', '')), reverse=True)
        for user in users:
            print(user)
            spamwriter.writerow(user.getMembersAsArray())

        # p.extractHighScore()
        # for i in users:
        #     print('printing: ' + i.first)
        #     print(i)
        # print(p.headers)
        # print(csv.Sniffer().has_header(csvfile.read(1024)))
