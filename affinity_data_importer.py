import sys
import wrapper
if __name__ == '__main__':
    arguments = sys.argv

    if len(arguments) < 3:
        print('The program requires at least two arguments.\nExample: affinity_data_importer.py inputfile.csv outputfilename.csv')
        exit(0)

    input_file = arguments[1]
    output_file = arguments[2]

    wrapper.runImporter(input_file, output_file)

    exit(0)

